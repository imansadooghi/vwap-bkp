/*******************************************************************************
* File helperFunctions.h
 * Goal: Provides helper functions

* Created May 9th  - Iman Sadooghi
* (C) 2018
******************************************************************************/

#ifndef VWAP_HELPERFUNCTIONS_H
#define VWAP_HELPERFUNCTIONS_H
/******************************************************************************
*include files
******************************************************************************/
#include <string>
/******************************************************************************
*Class helperFunctions
******************************************************************************/
class HelperFunctions {
public:
/******************************************************************************
*Constructor
******************************************************************************/
  HelperFunctions() = default;
/******************************************************************************
*Destructor
******************************************************************************/
  virtual ~HelperFunctions() = default;
/******************************************************************************
*Functions
******************************************************************************/

/******************************************************************************
*Function parseIPString()
 * Goal: Extracts IP and port number from a string of IP:PORT
 * Input arguments: std:: string
 * Return: std::vector<std::string> IP and Port number
******************************************************************************/
  std::vector<std::string> parseIPString(std::string ip);

/******************************************************************************
*Function parseUserInput()
 * Goal: Parses user input
 * Input arguments: char** user_input
 * Return: 0 if no errors, otherwise an error code
******************************************************************************/
  int parseUserInput(char** user_input, std::vector<std::string>);


};


#endif //VWAP_HELPERFUNCTIONS_H
