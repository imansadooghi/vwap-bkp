//
// Created by anthony on 5/9/18.
//

#ifndef VWAP_STRUCTURES_H
#define VWAP_STRUCTURES_H

#include <string>
#include <cstring>
#include <array>
#include <utility>
#include <iostream>

/******************************************************************************
*UserInput structure
******************************************************************************/
struct UserInput{
  std::string _symbol;
  std::string _order_side;
  int _max_order;
  double _vwap_interval;
  double _order_timeout;
  std::string _market_data_ip_port;
  std::string _order_ip_port;

/*******************
*Constructor
*******************/
  UserInput(std::string symbol, std::string order_side,
            int max_order, double vwap_interval, double order_timeout,
            std::string market_data_ip_port,
            std::string order_ip_port) : _order_side(std::move
                                                                (order_side)),
                                                _max_order(max_order),
                                                _vwap_interval(vwap_interval),
                                                _order_timeout(order_timeout),
  _market_data_ip_port(std::move(
                                                    market_data_ip_port)),
                                                _order_ip_port(
                                                    std::move(order_ip_port)) {
    /*TODO: delete the initilizer list and initialize everything inside the body of the constructor. Perform checks input for any errors and print out correct usage*/
    _symbol = std::move(symbol);
    _order_side =std::move(order_side);
    if(symbol == nullptr){
      std::cerr << "Symbol is a string, Please try again"
                << "\n";
      exit(1);
    }

  }
/*******************
*Destructor
*******************/
  virtual ~UserInput() {}
};


/******************************************************************************
*Message structure
******************************************************************************/
struct Message{
  uint8_t message_type;
  uint64_t timestamp;
  std::array<char,8> symbol;
  int32_t bid_price;
  uint32_t bid_quantity;
  int32_t ask_price;
  uint32_t ask_quantity;

/*******************
*Constructor
*******************/
  Message(uint8_t message_type, uint64_t timestamp,
          const std::array<char, 8> &symbol, int32_t bid_price,
          uint32_t bid_quantity, int32_t ask_price, uint32_t ask_quantity)
      : message_type(message_type), timestamp(timestamp), symbol(symbol),
        bid_price(bid_price), bid_quantity(bid_quantity), ask_price(ask_price),
        ask_quantity(ask_quantity) {}

/*******************
*Destructor
*******************/
  virtual ~Message() {}

};

/******************************************************************************
*Priority Queue structure
******************************************************************************/
struct PriorityQueue{
  std::priority_queue<Message> message_priority_queue;
/*******************
*Constructor
*******************/
  PriorityQueue(const std::priority_queue<Message> &message_priority_queue)
      : message_priority_queue(message_priority_queue) {}
/*******************
*Destructor
*******************/
  virtual ~PriorityQueue() {}
};

/******************************************************************************
*Buffer structure
******************************************************************************/
struct Buffer{
  std::vector<char> input_buffer;
/*******************
*Constructor
*******************/
  Buffer(const std::vector<char> &input_buffer) : input_buffer(input_buffer) {}
/*******************
*Destructor
*******************/
  virtual ~Buffer() {}
};

#endif //VWAP_STRUCTURES_H
