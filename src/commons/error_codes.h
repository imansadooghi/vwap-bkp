/*******************************************************************************
* File helperFunctions.h
 * Goal: Provides helper functions

* Created May 9th  - Iman Sadooghi
* (C) 2018
******************************************************************************/

#ifndef VWAP_ERROR_CODES_H
#define VWAP_ERROR_CODES_H
/******************************************************************************
*error codes enum
******************************************************************************/
typedef enum error_codes{
  USER_INPUT_ERROR                    = 550,

} errorcodes;



#endif //VWAP_ERROR_CODES_H
