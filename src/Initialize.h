/*******************************************************************************
* File Initialize.h
 * Goal: Initializes the system and establishes connections with TCP ports

* Created May 9th  - Iman Sadooghi
* (C) 2018
******************************************************************************/
#ifndef VWAP_INIT_H
#define VWAP_INIT_H
/******************************************************************************
*include files
******************************************************************************/
#include <string>
#include <vector>
#include <queue>
#include <memory>
#include "commons/structures.h"

/******************************************************************************
*Class Initialize
******************************************************************************/
class Initialize {
public:
/******************************************************************************
*Constructor
******************************************************************************/
  Initialize(const UserInput* input){

    /* spawn 4 threads:
     * A) marketData(input.market_data_ip_port);//call constructor
     * B) Producer.init();
     * C) VWAPEngine.init();
     * D) orderData(input.order_ip_port);//call constructor


  }
/******************************************************************************
*Destructor
******************************************************************************/
  virtual ~Initialize() {}

/******************************************************************************
*Private members
******************************************************************************/

/******************************************************************************
*Function establishConnection()
 * Goal: Spawns a listener thread and connects to the given IP:PORT
 * Input arguments: std::string ip.port
 * Return: True if connection was successfully built, false otherwise
******************************************************************************/
  bool establishConnection(std::string ip);

};


#endif //VWAP_INIT_H
